# node need (no docker here)
`nvm use 18.17.0`

# start project
`npx create-next-app plannera`

# install svg

`yarn add @svgr/webpack`

next.config.mjs
```js
// next.config.mjs
export default {
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
};
```

# tailwind

## responsivo, tag pc/mobile
```
theme: {
  extend: {
    screens: {
      mobile: { max: "640px" },
      pc: { min: "641px" },
    },
  }
}
```

## cor

`tailwind.config.ts`

```
theme: {
  colors: {
    primary: "#2D3558",
    secondary: "#E89A3C",
    hoverSecondary: "#F7941E",
    red: "#dc564a",
    orange: "#e88411",
    green: "#2BA09D",
    white: "#FFF",
  },
}
```

## fonte

`./app/layout.tsx`

dentro do head
```tsx
        <link
          href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap"
          rel="stylesheet"
        />
```

`tailwind.config.ts`

```
extend: {
  screens: {
    mobile: { max: "640px" },
    pc: { min: "641px" },
  },
  fontFamily: {
    sans: ["Open Sans", "sans-serif"],
  },
},
```