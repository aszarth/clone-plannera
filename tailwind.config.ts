import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    colors: {
      primary: "#2D3558",
      secondary: "#E89A3C",
      hoverSecondary: "#F7941E",
      white: "#FFF",
      red: "#dc564a",
      orange: "#EA724A",
      yellow: "#F8B967",
      blue: "#3A4C8A",
      green: "#2BA09D",
      light_grey: "#CFD3D7",
      ultra_light_grey: "#E9EBED",
      black: "#000",
    },
    extend: {
      screens: {
        mobile: { max: "640px" },
        pc: { min: "641px" },
      },
      fontFamily: {
        sans: ["Open Sans", "sans-serif"],
      },
    },
  },
  plugins: [],
};
export default config;
