import Clients from "./components/Clients";
import Depositions from "./components/Depositions";
import Faq from "./components/Faq";
import Herocard from "./components/Herocard";
import Results from "./components/Results";
import Solutions from "./components/Solutions";

export default function Home() {
  return (
    <main>
      <Herocard />
      <Clients />
      <Results />
      <Solutions />
      <Depositions />
      <Faq />
    </main>
  );
}
