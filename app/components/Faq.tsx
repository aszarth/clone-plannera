"use client";
import { ReactNode, useState } from "react";
import Icon from "./Icon";

export default function Faq() {
  const [isOpenId, setIsOpenId] = useState(0);
  return (
    <section
      className={`bg-white pc:h-[800px] mobile:h-[${
        isOpenId == 0 ? "900px" : "1200px"
      }] mt-[50px] flex flex-col items-center`}
    >
      <h2 className="text-primary text-center text-[40px] font-extrabold">
        Dúvidas frequentes
      </h2>
      <div className="max-w-[800px] pc:mt-[50px] mobile:mt-[25px]">
        <QuestionCardCollapse
          title="O que é S&OP?"
          cardId={1}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            <span className="text-orange">
              O S&OP, Sales and Operations Planning
            </span>
            , é um processo de planejamento colaborativo a nível tático. Ele
            integra estrategicamente áreas diversas, como vendas, marketing e
            operações em torno de um mesmo número do{" "}
            <span className="text-orange">plano de demanda</span>, permitindo
            uma visão abrangente sobre os principais trade-offs envolvidos ao
            lidar com a incerteza da demanda futura do mercado.
          </p>
        </QuestionCardCollapse>
        <QuestionCardCollapse
          title="Quais são os benefícios do S&OP para a sua empresa?"
          cardId={2}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            Com a implementação do S&OP, você poderá tomar{" "}
            <span className="text-orange">decisões embasadas em dados</span> e
            fatos e terá as áreas de demanda mais integradas em torno de um
            mesmo objetivo. Você terá a capacidade de produção mais alinhada com
            a demanda do cliente. Isso resulta em redução de 5 a 15% de
            <span className="text-orange">estoque</span> e 15 a 40% de perda de
            vendas. Além disso, você terá de 10 a 30% de aumento na eficiência e
            5 a 30% no Forecast Accuracy. Ou seja, com o S&OP, você conseguirá
            alcançar melhores resultados financeiros, minimizando custos e
            otimizando a sua linha de produção.
          </p>
        </QuestionCardCollapse>
        <QuestionCardCollapse
          title="Por que o Planejamento de Vendas e Operações é tão importante na atualidade?"
          cardId={3}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            Com o S&OP, você terá uma visão melhor da demanda do mercado e áreas
            mais integradas, permitindo ajustes rápidos e eficientes na produção
            e distribuição, colocando a sua empresa um passo à frente,
            minimizando riscos, otimizando recursos e capitalizando
            oportunidades de crescimento.
          </p>
        </QuestionCardCollapse>
        <QuestionCardCollapse
          title="Como implementar S&OP na sua empresa?"
          cardId={4}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            Nossa equipe de especialistas trabalhará em parceria com a sua para
            definir papéis e responsabilidades, desenhar o processo e participar
            das primeiras reuniões auxiliando você nas primeiras rodadas.
            Combinaremos as melhores práticas do setor com a nossa experiência e
            soluções tecnológicas avançadas para estruturar um processo
            personalizado para o seu negócio. Confira nosso{" "}
            <span className="text-orange">
              guia prático de implantação do S&OP
            </span>{" "}
            em formato de e-book com dicas de como implementar esse processo na
            sua empresa!
          </p>
        </QuestionCardCollapse>
        <QuestionCardCollapse
          title="O S&OP é adequado para empresas de todos os tamanhos?"
          cardId={5}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            O S&OP é um processo mais comum em empresas de grande complexidade,
            normalmente de médio e grande porte. No entanto, pode ser adaptado
            para empresas menores, desenvolvendo uma estrutura mais simples para
            entender como se preparar para atender a demanda futura da melhor
            maneira possível.
          </p>
        </QuestionCardCollapse>
        <QuestionCardCollapse
          title="Como contratar o serviço de S&OP da Plannera?"
          cardId={6}
          isOpenId={isOpenId}
          setIsOpenId={setIsOpenId}
        >
          <p>
            <span className="text-orange">Entre em contato conosco</span> para
            uma conversa inicial, esclarecedora e sem compromisso. Nossa equipe
            estará pronta para ajudar e fornecer todas as informações
            necessárias.
          </p>
        </QuestionCardCollapse>
      </div>
    </section>
  );
}

function QuestionCardCollapse({
  title,
  children,
  cardId,
  isOpenId,
  setIsOpenId,
}: {
  title: string;
  children: ReactNode;
  cardId: number;
  isOpenId: number;
  setIsOpenId: Function;
}) {
  return (
    <section className="mt-[50px] px-[25px]">
      <div className="flex flex-row justify-between items-center">
        <h3
          className="cursor-pointer text-primary w-[90%] font-extrabold text-[24px] px-[10px]"
          onClick={() => {
            if (isOpenId == cardId) {
              setIsOpenId(0);
              return;
            }
            setIsOpenId(cardId);
          }}
        >
          {title}
        </h3>
        <div className="w-[32px] h-[32px] flex items-center justify-center">
          <CheckCircleIcon isSelected={Boolean(isOpenId == cardId)} />
        </div>
      </div>
      <div
        className={`transition-opacity duration-500 ease-in-out ${
          isOpenId == cardId ? "opacity-100" : "opacity-0 h-0 overflow-hidden"
        }`}
      >
        {children}
      </div>
    </section>
  );
}

function CheckCircleIcon({ isSelected }: { isSelected: boolean }) {
  const ballBgColor = isSelected ? "bg-white" : "bg-primary";
  const ballBorderColor = isSelected ? "border-primary" : "border-white";
  const ballBorderWidth = isSelected ? "border-2" : "border-1";
  const iconColor = isSelected ? "fill-primary" : "fill-white";

  return (
    <div
      className={`${ballBgColor} ${ballBorderColor} ${ballBorderWidth} rounded-full flex items-center justify-center w-[24px] h-[24px]`}
    >
      <Icon name="check" className={`${iconColor} w-[12px] h-[12px]`} />
    </div>
  );
}
