export default function Herocard() {
  return (
    <div className="bg-primary flex flex-row justify-center">
      <section className="grid grid-cols-10 gap-4 pc:px-[50px] mx-[20px] pc:max-w-[1300px] pc:h-[500px] mobile:h-[450px] w-full overflow-hidden pc:pt-[25px]">
        <div className="flex flex-col mobile:col-span-10 pc:col-span-5">
          <h2 className="mt-[15px] text-white mobile:text-[36px] pc:text-[50px] font-extrabold leading-tight tracking-tight">
            A solução completa para S&OP
          </h2>
          <p className="text-white mt-[15px] leading-tight tracking-tight">
            Queremos resolver o problema do mau planejamento{" "}
            <strong>por inteiro.</strong>
          </p>
          <p className="text-white mt-[10px] leading-tight tracking-tight">
            Nosso time multidisciplinar e nossas tecnologias proprietárias têm
            tudo o que você precisa para levar o seu{" "}
            <strong>S&OP/IBP para o próximo nível.</strong>
          </p>
          <p className="text-white mt-[10px] leading-tight tracking-tight">
            Tudo isso <strong>em um só lugar.</strong>
          </p>
          <button className="mt-[25px] text-white bg-secondary hover:bg-hoverSecondary w-[240px] hover:w-[250px] hover:mr-[10px] h-[60px] hover:h-[70px] hover:mb-[10px] rounded-lg font-bold transition-all duration-300 ease-in-out">
            Vamos conversar!
          </button>
        </div>
        <div className="mobile:hidden pc:pt-[10px] pc:pl-[10px] pc:col-span-5">
          <video
            src="https://plannera.com/wp-content/uploads/2024/02/plannera___animacao-Original_1.mp4"
            poster="https://plannera.com/wp-content/uploads/2024/03/Prancheta-1@2x.png"
            controls
            className="rounded-lg shadow-lg w-[600px] h-[350px] object-cover"
          ></video>
        </div>
      </section>
    </div>
  );
}
