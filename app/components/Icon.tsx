"use client";
import SvgGithub from "./svgs/github.svg";
import SvgInternet from "./svgs/internet-explorer.svg";
import SvgAngleDown from "./svgs/angle-down.svg";
import SvgAngleRight from "./svgs/angle-right.svg";
import SvgAngleLeft from "./svgs/angle-left.svg";
import SvgBars from "./svgs/bars.svg";
import SvgInstagram from "./svgs/instagram.svg";
import SvgYoutube from "./svgs/youtube.svg";
import SvgLinkedin from "./svgs/linkedin-in.svg";
import SvgWhatsapp from "./svgs/whatsapp.svg";
import SvgCheck from "./svgs/check.svg";
import SvgQuoteLeft from "./svgs/quote-left.svg";
import SvgQuoteRight from "./svgs/quote-right.svg";

type nameIconComponent =
  | "internet"
  | "git"
  | "angle-down"
  | "angle-right"
  | "angle-left"
  | "bars"
  | "instagram"
  | "youtube"
  | "linkedin"
  | "whatsapp"
  | "check"
  | "quote-left"
  | "quote-right";
const iconComponents = {
  internet: SvgInternet,
  git: SvgGithub,
  "angle-down": SvgAngleDown,
  "angle-right": SvgAngleRight,
  "angle-left": SvgAngleLeft,
  bars: SvgBars,
  instagram: SvgInstagram,
  youtube: SvgYoutube,
  linkedin: SvgLinkedin,
  whatsapp: SvgWhatsapp,
  check: SvgCheck,
  "quote-left": SvgQuoteLeft,
  "quote-right": SvgQuoteRight,
};

export default function Icon({
  name,
  onClick,
  className,
}: {
  name: nameIconComponent;
  onClick?: Function;
  className?: string;
}) {
  const IconComponent = iconComponents[name];
  if (!IconComponent) return null;
  return (
    <div
      onClick={() => {
        if (onClick) onClick();
      }}
      className={className}
    >
      <IconComponent className={className} />
    </div>
  );
}
