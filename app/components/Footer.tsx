"use client";
import Icon from "./Icon";

export default function Footer() {
  return (
    <footer className="bg-primary pc:h-[300px] mobile:h-[500px] mt-[50px] flex items-center justify-center">
      <section className="mobile:flex mobile:flex-col max-w-[1100px] items-center justify-center">
        <h2 className="hidden">Footer</h2>
        <div className="pc:grid pc:grid-cols-3">
          <Logo />
          <Adress />
          <SocialMedias />
        </div>
        <CopyRight />
      </section>
    </footer>
  );
}

function Logo() {
  return (
    <section className="pt-[30px] pb-[10px] flex items-center justify-center">
      <h3 className="hidden">Logo Footer</h3>
      <img
        src="https://plannera.com/wp-content/uploads/elementor/thumbs/cropped-Plannera_LOGO_branca-p1pj9nie38uil4iowe8rc4hztkjm12qv09ion7ncvo.png"
        title="cropped-Plannera_LOGO_branca.png"
        alt="cropped-Plannera_LOGO_branca.png"
        className="pc:w-[300px] pc:h-[75px] mobile:w-[200px] mobile:h-[50px]"
      ></img>
    </section>
  );
}

function Adress() {
  return (
    <section className="text-white text-center">
      <h3 className="mobile:my-[20px] text-[20px] font-extrabold">
        Venha nos visitar
      </h3>
      <p className="mobile:my-[20px] p-[10px] leading-[1.0rem]">
        <strong className="font-extrabold">Rio de Janeiro</strong>
        <br />
        Av. Rio Branco, 181 – Sala 3103, Centro, Rio de Janeiro – RJ, CEP
        20040-007
      </p>
      <p className="mobile:my-[20px]">
        Tel: <span className="text-orange">+55 (21) 3529-7371</span>
      </p>
    </section>
  );
}

function SocialMedias() {
  return (
    <section className="flex flex-col justify-center items-center">
      <h3 className="text-white text-[20px] font-extrabold">Redes Sociais</h3>
      <div className="flex flex-row gap-2 my-5">
        <SocialMediaBall icon="instagram" />
        <SocialMediaBall icon="youtube" />
        <SocialMediaBall icon="linkedin" />
        <SocialMediaBall icon="whatsapp" />
      </div>
    </section>
  );
}
function SocialMediaBall({
  icon,
}: {
  icon: "instagram" | "youtube" | "linkedin" | "whatsapp";
}) {
  function HandleClick() {
    console.log("to-do");
  }
  return (
    <div className="w-[36px] h-[36px] rounded-full bg-white hover:bg-secondary transition-colors duration-300 ease-in-out flex items-center justify-center cursor-pointer">
      <Icon
        name={icon}
        onClick={HandleClick}
        className="w-[24px] h-[24px] fill-primary hover:fill-white"
      ></Icon>
    </div>
  );
}

function CopyRight() {
  return (
    <section className="flex flex-col items-center pc:mt-[25px]">
      <hr className="w-full text-white" />
      <h2 className="text-white pc:text-[14px] mobile:text-[12px] my-[15px]">
        Copyright © 2023 Plannera – Todos os direitos reservados
      </h2>
    </section>
  );
}
