"use client";
import Icon from "./Icon";
import CheckIsMobile from "../libs/CheckIsMobile";
import { useEffect, useState } from "react";

export default function Header() {
  const [isMobile, setIsMobile] = useState(false);
  useEffect(() => {
    setIsMobile(CheckIsMobile());
  }, []);
  return <header>{isMobile ? <HeaderMobile /> : <HeaderPC />}</header>;
}

function HeaderMobile() {
  return (
    <div className="bg-primary h-[70px] flex flex-row justify-between items-center">
      <Logo className="w-[150px] h-[40px] cursor-pointer ml-[20px]" />
      <Icon name="bars" className="fill-white w-[24px] h-[24px] mr-[40px]" />
    </div>
  );
}

function HeaderPC() {
  return (
    <div className="bg-primary h-[120px] flex items-center justify-center">
      <nav className="flex flex-row max-w-[1300px] w-full justify-between pr-[50px] pl-[50px]">
        <h2 className="hidden">Barra de Navegação principal</h2>
        {/* <h2 className="hidden">Navegação Principal</h2> */}
        <Logo className="w-[200px] h-[50px] cursor-pointer" />
        <ul className="flex flex-row items-center gap-[25px]">
          <NavHeaderItem dropdown={true}>Soluções</NavHeaderItem>
          <NavHeaderItem>Cases</NavHeaderItem>
          <NavHeaderItem dropdown={true}>Empresa</NavHeaderItem>
          <NavHeaderItem dropdown={true}>Conhecimento</NavHeaderItem>
          <ContactButton />
          <IdiomItem
            txt="PT"
            flag="https://plannera.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/pt_BR.png"
          />
          <IdiomItem
            txt="EN"
            flag="https://plannera.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/en_US.png"
          />
          <IdiomItem
            txt="ES"
            flag="https://plannera.com/wp-content/plugins/translatepress-multilingual/assets/images/flags/es_ES.png"
          />
        </ul>
      </nav>
    </div>
  );
}

function Logo({ className }: { className: string }) {
  return (
    <img
      className={className}
      src="https://plannera.com/wp-content/uploads/2021/01/cropped-Plannera_LOGO_branca.png"
      alt="Logo da Empresa"
    />
  );
}

function NavHeaderItem({
  children,
  dropdown,
}: {
  children: string;
  dropdown?: boolean;
}) {
  return (
    <li className="flex items-center cursor-pointer text-white fill-white transition-colors duration-300 ease-in-out hover:text-secondary hover:fill-secondary">
      <a className="text-base px-1">{children}</a>
      {dropdown && <Icon name="angle-down" className="w-[16px] h-[16px]" />}
    </li>
  );
}

function ContactButton() {
  return (
    <li>
      <button className="text-white bg-orange w-[100px] h-[40px] rounded-full cursor-pointe transition-all duration-300 ease-in-out hover:bg-red hover:mb-1">
        Contato
      </button>
    </li>
  );
}

function IdiomItem({ txt, flag }: { txt: string; flag: string }) {
  return (
    <li className="flex items-center justify-center cursor-pointer">
      <img
        src={flag}
        className="w-[18px] h-[12px]"
        alt={`bandeira-idioma-pais-${flag}`}
      />
      <span className="text-white ml-1 text-white fill-white transition-colors duration-300 ease-in-out hover:text-secondary hover:fill-secondary">
        {txt}
      </span>
    </li>
  );
}
