export default function Results() {
  return (
    <section className="bg-primary mt-[50px] pc:h-[650px] mobile:h-[360px] flex flex-col items-center">
      <div className="container mx-auto px-4">
        <h2 className="mobile:mt-[25px] pc:mt-[60px] text-[43px] leading-[2.25rem] text-white text-center font-extrabold break-words">
          Nossos resultados
        </h2>
      </div>

      <img
        src="https://plannera.com/wp-content/uploads/2023/04/Nossos-resultados-Plannera_3-2-2048x791.png"
        alt="imagem-grafico-resultados"
        className="pc:mt-[70px] mobile:mt-[50px] pc:w-[1000px] pc:h-[400px] mobile:w-[365px] mobile:h-[140px] flex align-center content-center"
      />

      <h3 className="hidden">
        Custo do Excesso: 5 a 15% de redução de <strong>Estoque</strong>
      </h3>
      <h3 className="hidden">
        Custo da Pressa: 10 a 30% de aumento de <strong>eficiência</strong>
      </h3>
      <h3 className="hidden">
        Custo da Falta: 10 a 40% de redução de <strong>Perdas de Vendas</strong>
      </h3>
      <h3 className="hidden">
        Incerteza: 5 a 30% de aumento no <strong>Forecast Accuracy</strong>
      </h3>
    </section>
  );
}
