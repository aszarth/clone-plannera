export default function Clients() {
  return (
    <div className="flex justify-center items-center bg-white w-[100vw]">
      <div className="max-w-[1300px]">
        <section>
          <div className="container mx-auto px-4">
            <h2 className="mobile:text-[35px] pc:text-[46px] font-extrabold mt-[50px] text-center text-primary pb-[25px] break-words w-full">
              Quem confia na gente
            </h2>
          </div>
          <div className="flex pc:flex-row mobile:flex-col pc:gap-20 mobile:gap-10">
            <CounterItem counterStr="+70">
              Implantações <strong>realizadas</strong>
            </CounterItem>
            <CounterItem counterStr="+4 anos">
              Duração média de <strong>parceria</strong>
            </CounterItem>
            <CounterItem counterStr="★ 9.1">
              Avaliação dos nossos <strong>clientes</strong>
            </CounterItem>
          </div>
          <section className="grid justify-items-center items-center pc:grid-cols-5 mobile:grid-clos-1 pc:gap-10 mobile:gap-12 mt-[50px]">
            <h3 className="hidden">Algumas Empresas Clientes</h3>
            {/* l1 */}
            <ClientCard
              name="Coca-Cola"
              url="https://plannera.com/wp-content/uploads/2023/04/Coca-cola.svg"
            />
            <ClientCard
              name="DSM"
              url="https://plannera.com/wp-content/uploads/2023/03/DSM_logo-1024x510.png"
            />
            <ClientCard
              name="Petrobras"
              url="https://plannera.com/wp-content/uploads/2023/04/Petrobras-cliente-Plannera.svg"
            />
            <ClientCard
              name="Silimed Silimed"
              url="https://plannera.com/wp-content/uploads/2023/03/Silimed_Silimed-logo.png"
            />
            <ClientCard
              name="Lavoro"
              url="https://plannera.com/wp-content/uploads/2023/03/Lavoro-1024x677.png"
            />
            {/* l2 */}
            <ClientCard
              name="Pierre-Fabre"
              url="https://plannera.com/wp-content/uploads/2023/04/Pierre-Fabre-cliente-Plannera.svg"
            />
            <ClientCard
              name="General-Mills"
              url="https://plannera.com/wp-content/uploads/2023/03/General-Mills-1024x597.png"
            />
            <ClientCard
              name="EMS"
              url="https://plannera.com/wp-content/uploads/2023/03/Ems-original.png"
            />
            <ClientCard
              name="Rumo"
              url="https://plannera.com/wp-content/uploads/2023/04/Rumo-cliente-Plannera.svg"
            />
            <ClientCard
              name="Iconic"
              url="https://plannera.com/wp-content/uploads/2023/03/logo_iconic-1024x313.png"
            />
            {/* l3 */}
            <ClientCard
              name="Bat"
              url="https://plannera.com/wp-content/uploads/2023/04/Bat-cliente-Plannera.png"
            />
            <ClientCard
              name="CSN"
              url="https://plannera.com/wp-content/uploads/2023/03/csn-logo-1-1024x464.png"
            />
            <ClientCard
              name="PUIG"
              url="https://plannera.com/wp-content/uploads/2023/04/puig.svg"
            />
            <ClientCard
              name="SIN"
              url="https://plannera.com/wp-content/uploads/2023/03/Sin.impante.png"
            />
            <ClientCard
              name="Boticario"
              url="https://plannera.com/wp-content/uploads/2023/03/boticario.png"
            />
          </section>
        </section>
      </div>
    </div>
  );
}

function CounterItem({
  counterStr,
  children,
}: {
  counterStr: string;
  children: React.ReactNode;
}) {
  return (
    <section className="flex flex-col items-center content-center">
      <p className="pc:text-[65px] mobile:text-[45px] font-extrabold text-primary">
        {counterStr}
      </p>
      <h3 className="pc:text-[20px] mobile:text-[12px] text-primary">
        {children}
      </h3>
    </section>
  );
}

function ClientCard({ url, name }: { url: string; name: string }) {
  return (
    <>
      <img
        src={url}
        alt={`imagem empresa cliente ${name}`}
        className="w-[120px] h-[40px]"
      />
    </>
  );
}
