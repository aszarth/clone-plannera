"use client";
import { ReactNode, useEffect, useState } from "react";
import Icon from "./Icon";

interface DepositionInterface {
  avatarSrc: string;
  name: string;
  role: string;
  content: ReactNode;
}

export default function Depositions() {
  const ContentLeonardo =
    "Quando se trabalha em planejamento há muitos anos, poder contar com um time especialista, pragmático e com experiência nos processos de negócio e não apenas em ferramentas que o suportam, traz segurança ao time gestor para a realização das transformações necessárias nos atuais modelos de planejamento de forma ágil e com foco no resultado desejado.";
  const ContentValdoberto = () => (
    <>
      <p>
        Conhecendo a tradicional abordagem de consultorias, diagnóstico e
        recomendações de grande dificuldade de implementação real, tínhamos
        certeza de que procurávamos algo bastante diferente quando encontramos a
        Plannera, e fomos muito bem sucedidos.
      </p>
      <br />
      <p>
        O modelo inovador de implementação prática e a quatro mãos das
        recomendações nos trouxe benefícios concretos e sustentáveis.
      </p>
    </>
  );
  const ContentNicolas = () => (
    <>
      <p>
        O principal ponto que ajudou nós, da Coca-Cola de Brasilia, a optar pela
        Plannera foi a parceria e auxílio na implementação do software da
        empresa.
      </p>
      <br />
      {/* <p>
        Buscamos várias soluções no mercado, mas todas tinham características
        rígidas para funcionar.{" "}
      </p>
      <br /> */}
      <p>
        O software da Plannera veio respeitando as nossas particularidades, mas
        trazendo grandes evoluções nos patamares da nossa acuracidade de
        demanda.
      </p>
    </>
  );
  const depositionsList: Array<DepositionInterface> = [
    {
      avatarSrc:
        "https://plannera.com/wp-content/uploads/2021/05/leonardo-lavoro.jpg",
      name: "Leonardo Cavalieri",
      role: "Gerente de Planejamento (S&OP) - Lavoro",
      content: ContentLeonardo,
    },
    {
      avatarSrc:
        "https://plannera.com/wp-content/uploads/2021/05/valdoberto-souzacruz.jpg",
      name: "Valdoberto Vidal",
      role: "Head of Supply Network Operations - Souza Cruz",
      content: <ContentValdoberto />,
    },
    {
      avatarSrc:
        "https://plannera.com/wp-content/uploads/2021/05/nicolas-santos-cocacola.jpg",
      name: "Nicolas Santos",
      role: "Gerente de Supply Chain - Coca Cola",
      content: <ContentNicolas />,
    },
  ];
  const [selectedViewCard, setSelectedViewCard] = useState(0);
  const [nextViewCard, setNextViewCard] = useState(1);
  useEffect(() => {
    if (selectedViewCard < depositionsList.length - 1) {
      setNextViewCard(selectedViewCard + 1);
    } else {
      setNextViewCard(0);
    }
  }, [selectedViewCard, depositionsList.length]);
  return (
    <section className="bg-primary pc:h-[700px] mobile:h-[825px] mt-[50px]">
      <h2 className="pt-[50px] text-white text-[40px] text-center font-extrabold">
        O que os nossos clientes dizem
      </h2>
      <div className="mt-[20px] flex flex-row pc:gap-10 items-center justify-center">
        <Icon
          name="angle-left"
          className="w-[64px] h-[64px] fill-light_grey cursor-pointer"
          onClick={() => {
            if (selectedViewCard <= 0) {
              setSelectedViewCard(depositionsList.length - 1);
            } else {
              setSelectedViewCard(selectedViewCard - 1);
            }
          }}
        />
        <DepositionCard
          avatarSrc={depositionsList[selectedViewCard].avatarSrc}
          name={depositionsList[selectedViewCard].name}
          role={depositionsList[selectedViewCard].role}
          content={depositionsList[selectedViewCard].content}
        />
        <div className="mobile:hidden">
          <DepositionCard
            avatarSrc={depositionsList[nextViewCard].avatarSrc}
            name={depositionsList[nextViewCard].name}
            role={depositionsList[nextViewCard].role}
            content={depositionsList[nextViewCard].content}
          />
        </div>
        <Icon
          name="angle-right"
          className="w-[64px] h-[64px] fill-light_grey cursor-pointer"
          onClick={() => {
            if (selectedViewCard < depositionsList.length - 1) {
              setSelectedViewCard(selectedViewCard + 1);
            } else {
              setSelectedViewCard(0);
            }
          }}
        />
      </div>
      <NavBallItems
        selectedViewCard={selectedViewCard}
        setSelectedViewCard={setSelectedViewCard}
      />
    </section>
  );
}

function DepositionCard({
  avatarSrc,
  name,
  role,
  content,
}: DepositionInterface) {
  return (
    <div className="w-[350px] pc:h-[430px] mobile:h-[480px] mt-[25px] bg-white rounded-lg p-[15px]">
      <div className="flex flex-row items-center">
        <img className="w-[91px] h-[91px] rounded-full" src={avatarSrc}></img>
        <div className="ml-[5px]">
          <p className="text-[18px] font-extrabold text-primary leading-[1.0]">
            {name}
          </p>
          <p className="text-[12px] mt-[5px] text-orange leading-[1.0] tracking-tight">
            {role}
          </p>
        </div>
      </div>
      <div className="mt-[20px] flex flex-col pc:h-[280px] mobile:h-[340px]">
        <Icon
          name="quote-left"
          className="w-[32px] h-[32px] pb-[10px] fill-light_grey self-start"
        />
        <div className="flex flex-col items-center justify-center h-full">
          <p className="text-[14px] text-primary leading-[1.5]">{content}</p>
        </div>
        <Icon
          name="quote-right"
          className="w-[32px] h-[32px] pt-[10px] fill-light_grey self-end"
        />
      </div>
    </div>
  );
}

function NavBallItems({
  selectedViewCard,
  setSelectedViewCard,
}: {
  selectedViewCard: number;
  setSelectedViewCard: Function;
}) {
  return (
    <div className="flex flex-row gap-5 justify-center mt-[50px]">
      <NavBallItem
        selected={Boolean(selectedViewCard === 0)}
        onClick={() => setSelectedViewCard(0)}
      />
      <NavBallItem
        selected={Boolean(selectedViewCard === 1)}
        onClick={() => setSelectedViewCard(1)}
      />
      <NavBallItem
        selected={Boolean(selectedViewCard === 2)}
        onClick={() => setSelectedViewCard(2)}
      />
    </div>
  );
}

function NavBallItem({
  selected,
  onClick,
}: {
  onClick: Function;
  selected?: boolean;
}) {
  const bgColor = selected ? "bg-white" : "bg-black";
  return (
    <button
      className={`w-[20px] h-[20px] rounded-full ${bgColor} cursor-pointer`}
      onClick={() => onClick()}
    ></button>
  );
}
