export default function Solutions() {
  return (
    <div className="flex justify-center">
      <section className="max-w-[1050px]">
        <h2 className="text-[38px] text-primary text-center font-extrabold mt-[50px]">
          Nossas soluções
        </h2>
        <div className="flex pc:flex-row mobile:flex-col items-center justify-center pc:gap-2 mobile:gap-8 mt-[50px]">
          <SoluctionCard
            color="red"
            name="Demanda"
            description="Veja o futuro das vendas com mais clareza."
            logoSrc="https://plannera.com/wp-content/uploads/2023/03/DEMANDA.svg"
          />
          <SoluctionCard
            color="orange"
            name="Estoque"
            description="Elimine faltas e excessos e otimize o seu capital de giro."
            logoSrc="https://plannera.com/wp-content/uploads/2023/03/ESTOQUES_2.svg"
          />
          <SoluctionCard
            color="yellow"
            name="Operação"
            description="Aumente a eficiência e enxergue gargalos com clareza."
            logoSrc="https://plannera.com/wp-content/uploads/2023/03/OPERACOES_1.svg"
          />
          <SoluctionCard
            color="blue"
            name="S&OP/IBP"
            description="Planeje e decida a longo prazo e mantenha o time alinhado."
            logoSrc="https://plannera.com/wp-content/uploads/2023/06/SOP_2.svg"
          />
          <SoluctionCard
            color="green"
            name="Inteligência
          de Dados"
            description="Decisões melhores com o poder da Inteligência Artificial."
            logoSrc="https://plannera.com/wp-content/uploads/2023/03/ID_1.svg"
          />
        </div>
      </section>
    </div>
  );
}

function SoluctionCard({
  color,
  name,
  logoSrc,
  description,
}: {
  color: "blue" | "red" | "orange" | "yellow" | "green";
  name: string;
  logoSrc: string;
  description: string;
}) {
  // tailwind não funciona com interpolação direta, então fiz esses dicionarios
  const colorBg = {
    blue: "bg-blue",
    red: "bg-red",
    orange: "bg-orange",
    yellow: "bg-yellow",
    green: "bg-green",
  };
  const colorText = {
    blue: "text-blue",
    red: "text-red",
    orange: "text-orange",
    yellow: "text-yellow",
    green: "text-green",
  };
  return (
    <div
      className={`${colorBg[color]} rounded-xl pc:w-[240px] pc:h-[320px] mobile:w-[320px] mobile:h-[320px]`}
    >
      <div className="h-[120px] flex justify-center">
        <img
          className="w-[120px] h-[120px]"
          src={logoSrc}
          alt={`img solução: ${name}`}
        />
      </div>
      <div className="h-[64px] flex items-center justify-center">
        <h3 className="text-[24px] font-extrabold leading-[1.6rem] text-white text-center">
          {name}
        </h3>
      </div>
      <p className="h-[70px] text-[16px] leading-[1.0rem] text-white text-center px-[20px]">
        {description}
      </p>
      <div className="h-[50px] flex items-center justify-center">
        <button
          className={`${colorText[color]} w-[110px] h-[35px] text-[13px] hover:w-[120px] hover:h-[45px] hover:text-[16px] transition-all duration-300 ease-in-out font-bold bg-white rounded-full`}
        >
          Saiba mais
        </button>
      </div>
    </div>
  );
}
